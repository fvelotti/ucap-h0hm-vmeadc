"""
High-level tests for the  package.

"""

import ucap_h0hm_vmeadc


def test_version():
    assert ucap_h0hm_vmeadc.__version__ is not None

import logging
from typing import List
import numpy as np

from ucap.common import (
    AcquiredParameterValue,
    Event,
    FailSafeParameterValue,
    ValueHeader,
    ValueType,
)
from ucap.common.context import (
    configuration,
    device_name,
    transformation_name,
    published_property_name,
)

logger = logging.getLogger(__name__)

def convert(event: Event) -> Optional[FailSafeParameterValue]:

    #this is a test
    logger = logging.getLogger(__name__)
    logger.info("testing  %s/%s", device_name, transformation_name)
    logger.info("this is a test")
    header = event.trigger_value.header if event.trigger_value else ValueHeader()

    beam_name = event.get_value("selector").value.get_value()

    #if bct_int <= configuration['threshold_BCT']:
    #    return []

    output_apv = AcquiredParameterValue(f'{device_name}/processed_data', header)

    adc_apv: AcquiredParameterValue = event.get_value("adc_acquisition").value
    adc_ch1, type_data = adc_apv.get_value_and_type("dataChannel1")
    adc_ch2, type_data = adc_apv.get_value_and_type("dataChannel2")
    adc_ch3, type_data = adc_apv.get_value_and_type("dataChannel3")
    adc_ch4, type_data = adc_apv.get_value_and_type("dataChannel4")

    #if len(pos) != len(proj):
    #    logger.warning(f"Sem {id_sem} contained inconsistent data")
    #    return []
    lacq_data = [adc_ch1,adc_ch2,adc_ch3,adc_ch4]
    for ich, ch in enumerate( configuration["channels"]):
        pname = configuration["channel_names"][ich]
        data = np.array(lacq_data[ich])
        output_apv.update_value(pname, data, ValueType.INT_ARRAY)
        output_apv.update_value( f"{pname}_min", data.min(), ValueType.INT )

    f_output_apv = FailSafeParameterValue(output_apv)

    return [f_output_apv]
